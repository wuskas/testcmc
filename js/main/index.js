var player = jwplayer;
var indexVideo = 3;
document.addEventListener('DOMContentLoaded', function(){
  jwplayer("video_1").setup({
    file: "video/pushnoy.webm",
    width: "100%",
    title: "video_1",
    autostart: true,
    mute: true,
    events : {
      onDisplayClick : function(ev){
        console.log(ev.type);
        deleteSteam("video_1")
      },
      onError: function(event) {
        jwplayer("video_1").setup({
          autostart: true,
          file: "video/Error.mp4",
          image: "img/error404.png",
          width: "100%",
          repeat: true,
          events : {
            onDisplayClick : function(ev){
              if (ev.type == "displayClick") {
                deleteSteam("video_2")
                return false;
              }
            },
          }
        });
      }
    }
  });

  jwplayer("video_2").setup({
    file: "rtmp://dev.wowza.longtailvideo.com/vod/_definst_/sintel/640.mp4",
    width: "100%",
    title: "video_2",
    autostart: true,
    mute: true,
    events : {
      onDisplayClick : function(ev){
        if (ev.type == "displayClick") {
          deleteSteam("video_2")
          return false;
        }
      },
      onError: function(event) {
        jwplayer("video_2").setup({
          autostart: true,
          file: "video/Error.mp4",
          image: "img/error404.png",
          width: "100%",
          repeat: true,
          events : {
            onDisplayClick : function(ev){
              if (ev.type == "displayClick") {
                deleteSteam("video_2")
                return false;
              }
            },
          }
        });
      }
    },

  });

  document.getElementsByClassName('video_item')[0].onclick = function(event) {
    if(event.target.className.indexOf('video_item') == -1) {
      return false;
    }
    createVideoStream(event.target)
  }
  document.getElementsByClassName('video_item')[1].onclick = function(event) {
    if(event.target.className.indexOf('video_item') == -1) {
      return false;
    }
    createVideoStream(event.target)
  }
  document.getElementsByClassName('video_item')[2].onclick = function(event) {
    if(event.target.className.indexOf('video_item') == -1) {
      return false;
    }
    createVideoStream(event.target)
  }
  document.getElementsByClassName('video_item')[3].onclick = function(event) {
    if(event.target.className.indexOf('video_item') == -1) {
      return false;
    }
    createVideoStream(event.target)
  }

});


function deleteSteam(id) {

  var question = confirm('Удалить поток?');
  if (question) {

    var videoDiv = document.getElementById(id)
    videoDiv.parentNode.removeChild(videoDiv)
  } else {
    console.log(id)
    return false
  }
}



function createVideoStream(obj) {
  var haveChildren = !!obj.children;
  if (haveChildren == false) {
    return false
  }
  var link = prompt("Вставте ссылку на видео", '');
  if(!link && link != null) {
    alert('Необходимо ссылка на видео или на стрим!');
    return false;
  } else if(link == null) {
    return false;
  } else {
    var videoItem = obj;
    console.log(obj)
    var videoDiv = document.createElement('div');
    videoDiv.id = "video_"+indexVideo;
    videoItem.appendChild(videoDiv)
    var mainDiv = document.getElementsByClassName('row');
    var buttonDiv = document.getElementsByClassName('creator');
    player("video_"+indexVideo).setup({
      file: link,
      title: "video_"+indexVideo,
      autostart: true,
      mute: true,
      width: "100%",
      events : {
        onDisplayClick : function(ev){
          deleteSteam(videoDiv.id)
        },
        onError: function(event) {
          jwplayer(videoDiv.id).setup({
            autostart: true,
            file: "video/Error.mp4",
            image: "img/error404.png",
            width: "100%",
            repeat: true,
            events : {
              onDisplayClick : function(ev){
                if (ev.type == "displayClick") {
                  deleteSteam(videoDiv.id)
                  return false;
                }
              },
            }
          });
        }
      }
    });
    indexVideo++;
  }
}
